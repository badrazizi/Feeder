plugins {
    id("com.android.library")
    id("kotlin-android")
}

android {
    compileSdkVersion(30)

    defaultConfig {
        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode(1)
        versionName("1.0")

        testInstrumentationRunner("android.support.test.runner.AndroidJUnitRunner")

    }

    buildTypes {
//        release {
//            minifyEnabled(false)
//            proguardFiles(getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro')
//        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

configurations.all {
    resolutionStrategy {
        failOnVersionConflict()

        val kotlin_version: String by project
        val okio_version: String by project

        force("com.squareup.okio:okio:$okio_version")
        force("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
        force("org.jetbrains.kotlin:kotlin-stdlib-common:$kotlin_version")
        force("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
        force("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version")
        force("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    }
}


dependencies {
    val kotlin_version: String by project
    val okhttp_version: String by project
    val moshi_version: String by project

    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
    api(platform("com.squareup.okhttp3:okhttp-bom:$okhttp_version"))
    api("com.squareup.okhttp3:okhttp")
    api("com.squareup.moshi:moshi:$moshi_version")

    // tests
    testImplementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
    testImplementation("junit:junit:4.12")
}
