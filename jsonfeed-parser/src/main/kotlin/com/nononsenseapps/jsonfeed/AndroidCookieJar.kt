package com.nononsenseapps.jsonfeed

import android.webkit.CookieManager
import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl

class AndroidCookieJar : CookieJar {
    private val manager = CookieManager.getInstance()

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        val urlString = url.toString()

        for (cookie in cookies) {
            if (cookie.expiresAt <= 0 || cookie.expiresAt < System.currentTimeMillis()) {
                manager.setCookie(urlString, "${cookie.name}=;")
            } else {
                manager.setCookie(urlString, cookie.toString())
            }

        }

        manager.flush()
    }

    override fun loadForRequest(url: HttpUrl): List<Cookie> {
        return getCookies(url)
    }

    fun getCookies(url: HttpUrl): List<Cookie> {
        val cookies = manager.getCookie(url.toString())

        return if (cookies != null && cookies.isNotEmpty()) {
            cookies.split(";").mapNotNull { Cookie.parse(url, it) }
        } else {
            emptyList()
        }
    }

    fun getCookiesAsString(url: String): String = manager.getCookie(url) ?: ""

    fun getCookiesAsString(url: HttpUrl): String = getCookiesAsString(url.toString())

    fun remove(url: String) {
        val cookies = manager.getCookie(url) ?: return

        cookies.split(";")
                .map { it.substringBefore("=") }
                .onEach { manager.setCookie(url, "$it=; Expires=Wed, 31 Dec 2025 23:59:59 GMT") }

        manager.flush()
    }

    fun remove(url: HttpUrl) {
        remove(url.toString())
    }

    fun removeAll() {
        manager.removeAllCookies {}
        manager.flush()
    }

    fun sync() {
        manager.flush()
    }
}